********************************************************************************
* Programme       : texaux.ado                                                 *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 09.07.2015                                                 *
********************************************************************************

*! latex v1.0.0 JRanstam 18jul2015
program define texaux
  version 14

/* 
Syntax: texaux --option [texfile]

Where --option is one of

      --start texfile  
      --title "name" "author" "date"
      --contents (ToC)
      --footer

      --table head title (in cm) colpos colname
      --table row data
      --table end footnote

      --figure title width pngfile
      
      --section title
      --subsection title
      --text "text"
      --equation formula (LaTex code)
      --vspace distance (in cm)
      --newpage

      --include logfile

      --stop
      --exec texdir texfile pdfdir
*/

// Start of a new document
   if "`1'"=="--start" {
      tex \documentclass[12pt,a4paper]{article} 
      tex \usepackage[utf8]{inputenc}
      tex \usepackage{amsmath}
      tex \usepackage{amsfonts}
      tex \usepackage[margin=0.5in]{geometry}
      tex \usepackage{amssymb}
      tex \usepackage{graphicx}
      tex \usepackage{epstopdf}
      tex \usepackage{caption}
      tex \usepackage[labelformat=empty]{caption}
      tex \usepackage[T1]{fontenc}
      tex \usepackage[swedish]{babel}
      tex \usepackage{listings}
      tex \usepackage{courier}
      tex \lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
      tex \usepackage[ddmmyyyy]{datetime}
      tex \usepackage{fancyhdr}
      tex \date{\today}
      tex \begin{document}
      tex \renewcommand{\dateseparator}{.}
      tex \let\stdsection\section
      tex \let\stdsubsection\subsection
   }
// Title
   else if "`1'"=="--title"{
      tex \title{`2'}
      tex \author{`3'}
      tex \date{`4'}
      tex \clearpage\maketitle
      tex \thispagestyle{empty}
      tex \newpage
   }
// Contents
   else if "`1'"=="--contents"{
      tex \renewcommand{\contentsname}{Content}
      tex \tableofcontents
      tex \newpage
   }
// Footer
   else if "`1'"=="--footer"{
      tex \pagestyle{fancy}
      tex \renewcommand{\headrulewidth}{0pt}
      tex \rhead{\thepage}
      tex \cfoot{\scriptsize{`2'}}
   }
// Figures
   else if "`1'"=="--figure"{
      tex \begin{figure}[ht!]
      tex \includegraphics[width=`2'mm]{`3'}
      tex \label{overflow}
      tex \end{figure}
   }
   else if "`1'"=="--table"{
      if "`2'"=="head" {
	 local colpos="`3'"
 	 local colname="`4'"
         tex \begin{table}[h]
         tex \begin{tabular}{`colpos'}
         tex \hline
         tex `colname' \\ [0.5ex]
         tex \hline
      }
      if "`2'"=="row" {
         local data="`3'"
         tex `data' \\
      }
      if "`2'"=="end" {
         local footnote="`3'"
         tex \hline 
         tex \end{tabular}
         tex \end{table}
         tex \flushleft
         tex \vspace{-0.8cm}
         tex \scriptsize{`footnote'}
         tex \vspace{0.6cm}
      }
   }
// Section
   else if "`1'"=="--section"{
      local title="`2'"
      tex \section{`title'}   
   }
// Subsection   
      else if "`1'"=="--subsection"{
      local title="`2'"
      tex \subsection*{`title'}   
   }
// Vertical space
   else if "`1'"=="--vspace" {
      tex \vspace{`2'cm}"
   }
// Text
   else if "`1'"=="--text" {
      tex `2'   
   }
// Equation
   else if "`1'"=="--equation" {
      tex \begin{equation*}
      tex `2'   
      tex \end{equation*}
   }
// Include
   else if "`1'"=="--include" {
      tex \lstinputlisting{`2'}   
   }
// New page
   else if "`1'"=="--newpage"{
      tex \newpage       
   }
// End of document
   else if "`1'"=="--stop" {
      tex \end{document}
   }
// Create pdf document
   else if "`1'"=="--exec" {
      qui normalizepath `2'
      local file = r(filename)
      local dir = subinstr("`2'","`file'","",1)
      shell cd "`dir'" && pdflatex -output-directory="`dir'" "`2'"
   }
  
end
