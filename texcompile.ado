********************************************************************************
* Programme       : texcompile.ado                                             *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 02.08.2016                                                 *
********************************************************************************

*! texincl  v1.0.0 JRanstam 02aug2016

program define texcompile
   version 14
      
   shell pdflatex "`1'"

end
