********************************************************************************
* Programme       : texstop.ado                                                *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 12.06.2016                                                 *
********************************************************************************

*! texstop v1.0.0 JRanstam 12jun2016
program define texstop
   version 14

/* 
Syntax: texstop
*/

   tex \end{document}
  
end
