********************************************************************************
* Programme       : texpackage.ado                                             *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 21.08.2016                                                 *
********************************************************************************

*! texpackage  v1.0.0 JRanstam 20aug2016

program define texpackage
   version 14      

   if ("`c(os)'"=="Windows") texdoc close 
   file open utfil using "tmp.txt", write replace
   file open infil using "`1'", read text
    
   file read infil line
   while (r(eof)==0) {
      if ("`line'"=="\begin{document}") file write utfil "\usepackage{`2'}" _n
      file write utfil "`line'" _n
      file read infil line
   }

   file write utfil "`line'" _n

   file close utfil
   file close infil

   erase "`1'"
   copy "tmp.txt" "`1'"
   erase "tmp.txt"

   if ("`c(os)'"=="Windows") texdoc init "`1'", append
end





