********************************************************************************
* Programme       : texfooter.ado                                              *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 01.08.2016                                                 *
********************************************************************************

*! texfooter  v1.0.0 JRanstam 1aug2016

program define texfooter
   version 14
   
   texaux --footer "`1'"
   
end


