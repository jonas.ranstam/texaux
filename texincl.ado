********************************************************************************
* Programme       : texincl.ado                                                *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 01.08.2016                                                 *
********************************************************************************

*! texincl  v1.0.0 JRanstam 1aug2016

program define texincl
   version 14
      
   file open myfile using "`1'", read
   file read myfile line
   local output=""
   while r(eof)==0 {
      local output="`output'"+"`line'"
      file read myfile line
   }
   file close myfile
   tex `output'

end
