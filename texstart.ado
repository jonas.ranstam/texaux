********************************************************************************
* Programme       : texstart.ado                                               *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 12.06.2016                                                 *
********************************************************************************

*! texstart v1.0.0 JRanstam 12jun2016

program define texstart
   version 14
   
   texaux --start
   
end


