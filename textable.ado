********************************************************************************
* Programme       : textable.ado                                               *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 12.06.2016                                                 *
* Revised date    : 27.07.2016                                                 *
********************************************************************************

*! textable v1.0.0 JRanstam 12jun2016

program define textable
   version 14
   
   args class mod 

   preserve

   if (`class'==1) {
   
      qui:ds
      local vars=r(varlist)
      local varsl=subinstr("`vars'"," "," & ",.)

      qui: des
      local nvar=r(k)
      if (`mod'==0) {
         local cols = "l "
         forvalues i=2(1)`nvar' {
            local cols = "`cols'"+"r "
         }
      }
      else {
         local colcode=string(`mod')
         local ncol=strlen("`colcode'")
         forvalues i=1(1)`ncol'{
            if (real(substr("`colcode'",`i',1))==1) local c=" l"
            if (real(substr("`colcode'",`i',1))==2) local c=" c"
            if (real(substr("`colcode'",`i',1))==3) local c=" r"
            local cols = "`cols'"+"`c'" 
         }
      }
      texaux --table head "`cols'" "`varsl'"
      local nobs=_N
      forvalues i=1(1)`nobs' {
         local irow = ""
         foreach var of varlist `vars' {
            local irow = "`irow'"+`var'[`i']+" & "
         }
         local irow=substr("`irow'",1,strlen("`irow'")-3)
         texaux --table row "`irow'"
      }
   }
   if (`class'==2 | `class'==3) {

      texaux --table head "l r r c" "Variable & Effect & p-value & 95\% Ci"

      matrix avg=e(g_avg)
      local df = `mod'
      covars
      local covars = r(covars)

      foreach vars in `covars' {
         local b  = _b[`vars']
         local se = _se[`vars']
         pcalc `b' `se' `df' 2
         local p : di %8.4f r(p)
         if (`class'==2) {
            local b : di %8.2f r(b)
            local l : di %8.2f r(ll95)
            local u : di %8.2f r(ul95)
         }
         if (`class'==3) {
            local b : di %8.2f exp(r(b))
            local l : di %8.2f exp(r(ll95))
            local u : di %8.2f exp(r(ul95))
         }
         local cvarc=subinstr("`vars'","#","\#",.)
         texaux --table row "`cvarc' & `b' & `p' & `l' to `u'"
      }
   }

   if (`class'==4) {
      
      texaux --table head "l r r c" "Variable & Effect & p-value & 95\% Ci"

      matrix avg=e(g_avg)
      local df=99999
      covars
      local covars = r(covars)
      mat b  = e(b)   
      mat se = e(se)

      local j=0
      foreach vars in `covars' {
         local j=`j'+1
         if (`j'==1) texaux --table row " & & & "
         if (`j'==(e(df_m)/2)+1) texaux --table row " & & & "
         if (`j'==1) texaux --table row "\textbf{Component 1} & & & "
         if (`j'==(e(df_m)/2)+1) texaux --table row "\textbf{Component 2} & & & "

         if (`j'<=(e(df_m)/2)) local b = _b[component1:`vars']
         if (`j'<=(e(df_m)/2)) local se = _se[component1:`vars']
         if (`j'> (e(df_m)/2)) local b = _b[component2:`vars']
         if (`j'> (e(df_m)/2)) local se = _se[component2:`vars']
 
         pcalc `b' `se' `df' 2
         local b : di %8.2f r(b)
         local p : di %8.4f r(p)
         local l : di %8.2f r(ll95)
         local u : di %8.2f r(ul95)
         local cvarc=subinstr("`vars'","#","\#",.)
         texaux --table row "`cvarc' & `b' & `p' & `l' to `u'"
      }
   }
   restore
   
   texaux --table end
   tex \normalsize
   
end


