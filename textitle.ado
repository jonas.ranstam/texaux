********************************************************************************
* Programme       : textitle.ado                                               *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 01.08.2016                                                 *
********************************************************************************

*! textitle  v1.0.0 JRanstam 1aug2016

program define textitle
   version 14
   
   texaux --title "`1'" "`2'" "`3'"
   
end


