********************************************************************************
* Programme       : texfigure.ado                                              *
* Programmer      : Jonas Ranstam                                              *
* Programmed date : 12.06.2016                                                 *
********************************************************************************

*! texfigure v1.0.0 JRanstam 12jun2016

program define texfigure
   version 14
   
   args size path

   texaux --figure `size' "`path'"
   tex \normalsize
   
end


